# Introduction

The Transparent Water Initiative is an initiative to create a global website for water quality, starting locally.

## Why?

Water is essential to life. It should be accessible to anyone [[1](http://www.un.org/sustainabledevelopment/water-and-sanitation/)] but there is a this is lot unclear about water. And it is getting harder for the general public to be informed about water. In some countries in Europe, tap water quality is an issue. In some countries, tap water is fine, but big companies tell you that bottled water is better.[[2](https://www.youtube.com/watch?v=oR_KXZZc13U)]

The project _What's in my Water_ has been in progress at Code for Ireland for a while now, but we are now moving it to a European level. It will provide more transparency, insight and knowledge about water. We are building on the [_Was steckt in meinem Leitungswasser?_](http://codefor.de/projekte/2014-03-22-hn-trinkwasser) project that originates from Code for Germany (City of Heilbronn and City of Mannheim).

- [Because some want us to believe water is not a human right](https://www.youtube.com/watch?v=Zhm69FQr43A)
- Because everybody has the right to transparent insight in their water quality, [before something goes wrong](https://www.youtube.com/watch?v=wH9EGugC_qo)
- Because maybe [bottled water](https://www.youtube.com/watch?v=Se12y9hSOM0) is not the answer.

## What we want to build

- A generic, scalable geo-aware (data)model with an API to hold all information about:
  - Water nutrient and chemical contents
  - Quality
  - Suppliers
  - Waterwork operators
  - Zones for tap-water
  - Pumping stations 
  - ..and more as we develop

- A viewer where you can get insight on water and the ingredients and that allows you to compare tap with bottled, tap with tap (in other countries) regarding contents, quality and cost and that will answer questions like: When I am on vacation/travel, where can I drink from the tap?

## Infrastructure

- We are setting up a prototype database that can be reused, queried and used to create visualisations.

## What we need
- Database skills
- translators (We are using transifex and translate static html while compiling the client)
- Application creators, Frontend/UX people
- General thinkers/developers that want to help set up a informative and usable open data initiative
- General thinkers/developers that want to make sure a common user gets the right experience
- People that want to learn

## Convinced?

Join the development!

Concept sketches

![draw.io sketch](images/transparant_water_milo_diagram.png)

![Sketch by Duncan Healy](images/IMG_20170323_115229.jpg)