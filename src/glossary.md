# Glossary

## Code

An entity that can be observed in water, either visual or via metering techniques. Codes are derived from standards like ISO, EC, UN, ICSC. Examples of Code are:

- natrium
- calcium
- hardness

## Company

A business involved in producing and/or distributing water and/or involved in drinking water in any sense of the word (for instance, providing taps or generating water quality reports)


## Country

Used to determine which limit standard is applicable.

## Limit

A given standard for the maximum allowed quantity of observations of a certain type. There are different limits for instance for Europe and the US. Other limits can be added.

## Location

A location relevant to the system. Can be a Tap, Pump, Facility or Measurement location. Has information about access (public, restricted, commercial)

## Observation

An observation of quantities of a certain entity, given in unit of measure. Observations can be part of limits, reports and products.

## Product

A bottled water product. Observations on a product are what people can read from labels or measure themselves with a device.

## Report

An (official) report regarding water quality in a certain zone or on a location.

## Uom

Unit of measure. For instance: milligrams per liter (mg/L).

## User

Anybody or anything (device authorised to use the API) with access to the system.

## Zone

An area that is serviced by a single Drinkwater-facility. Should be a polygon that encloses all the piping running from the given facility.