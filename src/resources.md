# Resources

On this page we will gather interesting resources.

## General
* [Pubchem](https://pubchem.ncbi.nlm.nih.gov/) a search engine for chemicals, compounds and substances (Used to get CAS standard codes)
* A lot about bottled Water on [OpenFoodFacts](https://world.openfoodfacts.org/category/waters)

## North-America
* [California Water Service](https://www.calwater.com/waterquality/water-quality-reports/)
* [Conneticut Water](https://www.ctwater.com/water-quality/water-quality-report)

## Europe
* [WISE - Groundwater](https://data.europa.eu/euodp/en/data/dataset/data_wise-groundwater)
* [Waterbase](http://www.eea.europa.eu/data-and-maps/data/waterbase-water-quantity-9)

### Belgium# Resources

* [De Watergroep](https://www.dewatergroep.be)
* [Vivaqua](http://www.vivaqua.be)
* [PIDPA](http://www.pidpa.be)
* [Water-link](http://www.water-link.be)
* [Farys](http://www.farys.be)

### France
* [Volvic](http://www.volvic.fr)
* [Vittel](http://www.nestle-waters.com/brands/vittel)

### Germany
* Drinkingwater quality [report](https://www.offenesdatenportal.de/dataset/27f0606c-ac3d-4636-b2dc-f8bd302cd86c/resource/9fe31961-76e0-423a-8b54-965e8d262289/download/trinkwasser.csv) as .csv for Moers Wasserwerk Wittfeldstraße.
* [Wüteria Mineralquellen GmbH & Co. KG](http://wueteria.de)
* [Teusser Mineralbrunnen Karl Rössle GmbH & Co KG](http://www.teusser.de)

### Ireland
* [Irish water](https://www.water.ie)

### Netherlands
* Groundwater information [WFS service](https://data.overheid.nl/data/dataset/grondwaterbeschermingsgebieden-01-02-03-04)
* [Evides](http://www.evides.nl)
* [Dunea](http://www.dunea.nl)
* [Brabant Water](http://www.brabantwater.nl)
* [Oasen](http://www.oasen.nl)
* [Vitens](http://www.vitens.nl)
* [Waternet](http://www.waternet.nl)
* [WMD](http://www.wmd.nl)
* [Waterbedrijf Groningen](http://www.waterbedrijfgroningen.nl)
* [PWN](http://www.pwn.nl)
* [WML](http://www.wml.nl)
* [Het Waterlaboratorium](http://www.hetwaterlaboratorium.nl)
* [Aqualab Zuid](http://www.aqualab.nl)

#### Aruba
* [WEB N.V.](https://www.webaruba.com)

#### Bonaire
* [Water- en energiebedrijf Bonaire](https://www.webbonaire.com)

#### Curacao
* [AquaElectra](http://www.aqualectra.com)

#### Sint Maarten
* [NV GEBE](https://www.nvgebe.com)

### Poland

### Romania

### United Kingdom

* [Water company boundaries](http://neighbourhoods.esd.org.uk/#?areaType=WATER&overlayType=WATER&tab=Map) need to fetch the WFS and get the data.

## International
* [Join The Pipe](http://join-the-pipe.org) they have a javascript file with [markers](http://join-the-pipe.org/uploads/markers.js) for Outlets, Restaurants, Tap stations and pumps across the world.